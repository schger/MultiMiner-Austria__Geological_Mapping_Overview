# MultiMiner, Austria — Geological Mapping Overview

This is the git repository for the [MultiMiner](https://www.multiminer.eu/) field guide for the geological mapping in Hochfilzen, Austria.\
The guide is available in [English](https://gitea.geologie.ac.at/schger/MultiMiner-Austria__Geological_Mapping_Overview/src/branch/master/MuMi_AT__MappingGuide__EN.pdf) and [German](https://gitea.geologie.ac.at/schger/MultiMiner-Austria__Geological_Mapping_Overview/src/branch/master/MuMi_AT__MappingGuide__DE.pdf).

## Getting the source 
You can clone this repository using:

```bash
git clone https://gitea.geologie.ac.at/schger/MultiMiner-Austria__Geological_Mapping_Overview.git
```

Alternatively you can [download](https://gitea.geologie.ac.at/schger/MultiMiner-Austria__Geological_Mapping_Overview/archive/master.zip) it if you don't use git.

## Building the documents

The two versions of this field guide are stored in two files:

1. English: `MuMi_AT__MappingGuide__EN.qmd`
2. German: `MuMi_AT__MappingGuide__DE.qmd`

The documents are rendered using _quarto_:

```bash
quarto render MuMi_AT__MappingGuide__EN.qmd
```

or

```bash
quarto preview MuMi_AT__MappingGuide__EN.qmd
```

The first command just renders the document to a PDF file. The latter command renders the document and opens up a browser window showing the built PDF.

Alternatively — and more comfortably — _quarto_ can be used with diverses editors resp. IDEs like _VS Code_, _RStudio_, _Jupyter Lab_, ...

### Prerequisites

You need to have the following things installed before you can build the documents:

* A [LaTeX](https://www.latex-project.org/) distribution

  This depends on the system you are using:
  * Linux and similar systems: Check your distribution's repository, usually a [TeX Live](https://www.tug.org/texlive/) distribution can be installed directly.
  * Windows: [MiKTeX](https://miktex.org/) can be recommended or use [TeX Live](https://www.tug.org/texlive/).
  * Mac OS: [MacTeX](https://www.tug.org/mactex/) will meet all your needs.
* [quarto](https://quarto.org/) -- see the _Get Started_ guide there

The documents are created using the [quarto_titlepages](https://github.com/nmfs-opensci/quarto_titlepages) extension, which is included in this repository.
